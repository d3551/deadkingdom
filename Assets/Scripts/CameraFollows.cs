using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollows : MonoBehaviour
{
    [SerializeField] private float timeOffset;
    [SerializeField] private Vector3 posOffset;

    private Vector3 velocity;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position + posOffset, ref velocity, timeOffset);
    }
}