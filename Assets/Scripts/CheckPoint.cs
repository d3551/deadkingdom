using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    //private Transform playerSpawn;

    //private void Awake()
    //{
    //    playerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("PlayerSpawn").transform.position = transform.position;
            gameObject.GetComponent<Collider2D>().enabled = false;
            Destroy(gameObject);
        }
    }
}
