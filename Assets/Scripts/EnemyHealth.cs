using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] float maxHealth = 100f;
    private bool canTakeDamage = true;
    float currentHealth;
    [SerializeField] GameObject potion;
    private Animator anim;

    [SerializeField] private AudioSource hitAudio;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Reduce life
    public void TakeDamage(float damage)
    {
        if (canTakeDamage)
        {
            hitAudio.Play();
            if (currentHealth - damage <= 0)
            {
                Die();
            }

            if (currentHealth - damage > 0)
            {
                anim.SetTrigger("IsHit");
                currentHealth -= damage;
            }
        }
    }

    // Launch death animation and disable then destroy
    void Die()
    {
        canTakeDamage = false;
        anim.SetTrigger("IsDead");
        GetComponent<Collider2D>().enabled = false;
        this.gameObject.GetComponent<EnemyPatrol>().canMove = false;
        this.enabled = false;

        StartCoroutine(Despawn(anim.GetCurrentAnimatorStateInfo(0).length - 0.1f));
    }

    // Destroy object after some time
    public IEnumerator Despawn(float time)
    {
        yield return new WaitForSeconds(time);
        if (Random.Range(0,10)>5)
        {
            Instantiate(potion, this.gameObject.transform.position, this.gameObject.transform.rotation);
        }
        Destroy(this.gameObject);
    }
}
