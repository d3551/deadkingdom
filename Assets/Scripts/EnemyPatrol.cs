using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float damage = 10;
    [SerializeField] private float attackDamage = 20;
    [SerializeField] private float attackDistance = 1f;
    [SerializeField] private Transform sightPoint;
    [SerializeField] private Transform[] waypoints;
    [SerializeField] private SpriteRenderer spriteRenderer;
    private Animator anim;

    private Transform target;
    private Transform player;
    private int destPoint = 0;
    private bool moveToPoint = true;
    public bool canMove = true;

    [SerializeField] Transform attackPoint;
    [SerializeField] float attackRange = 0.4f;
    [SerializeField] LayerMask playerLayers;

    [SerializeField] float attackRate = 2f;
    //float nextAttackTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = waypoints[0];
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (moveToPoint)
        {
            StopCoroutine(Attack());
        }
        if (canMove)
        {

            if (moveToPoint)
            {
                Vector3 dir = new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
                transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
                if (dir.x > 0 && this.transform.localScale.x < 0 || dir.x < 0 && this.transform.localScale.x > 0)
                {
                    Flip();
                }
                if (Vector3.Distance(transform.position, target.position) < 0.3f)
                {
                    destPoint = (destPoint + 1) % waypoints.Length;
                    target = waypoints[destPoint];
                }
            }
            else
            {
                Vector3 goal = new Vector3(player.position.x - transform.position.x, 0, player.position.z - transform.position.z);
                

                if (goal.x > 0 && this.transform.localScale.x < 0 || goal.x < 0 && this.transform.localScale.x > 0)
                {
                    Flip();
                }
                
                if (Vector3.Distance(transform.position, player.position) < attackDistance)
                {
                    StartCoroutine(Attack());
                }
                else
                {
                    transform.Translate(goal.normalized * speed * Time.deltaTime, Space.World);
                }
            }
        }
    }

    void Flip()
    {
        //Vector3 attack = attackPoint.localPosition;
        //Vector3 sight = sightPoint.localPosition;
        //spriteRenderer.flipX = !spriteRenderer.flipX;
        //attack.x *= -1;
        //sight.x *= -1;
        //sightPoint.localPosition = sight;
        //attackPoint.localPosition = attack;
        this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, this.transform.localScale.z);
    }

    public void MoveToPlayer(bool move)
    {
        moveToPoint = !move;
    }

    public IEnumerator Attack()
    {

        canMove = false;
        anim.SetTrigger("IsAttacking");
        yield return new WaitForSeconds(0.3f);
        Collider2D[] hitPlayers = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayers);
        foreach (Collider2D player in hitPlayers)
        {
            player.GetComponent<PlayerHealth>().TakeDamage(attackDamage);
        }

        yield return new WaitForSeconds(attackRate);
        canMove = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            PlayerHealth playerHealth = collision.transform.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
        }
    }

    void OnDrawGizmos()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
