using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            this.transform.parent.GetComponent<EnemyPatrol>().MoveToPlayer(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            this.transform.parent.GetComponent<EnemyPatrol>().MoveToPlayer(false);
        }
    }
}
