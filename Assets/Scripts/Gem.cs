using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gem : MonoBehaviour
{

    [SerializeField] private AudioSource gemAudio;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            gemAudio.Play();
            SceneManager.LoadScene("MainMenu");
        }
    }
}