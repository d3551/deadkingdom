using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject optionsUI;
    [SerializeField] private GameObject gamepadUI;
    [SerializeField] private GameObject keyboardUI;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Options()
    {
        if (!optionsUI.activeInHierarchy)
        {
            optionsUI.SetActive(true);
        }
        else
        {
            optionsUI.SetActive(false);
        }
    }

    public void Change()
    {
        if (gamepadUI.activeInHierarchy)
        {
            gamepadUI.SetActive(false);
            keyboardUI.SetActive(true);
        }
        else if (keyboardUI.activeInHierarchy)
        {
            gamepadUI.SetActive(true);
            keyboardUI.SetActive(false);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
