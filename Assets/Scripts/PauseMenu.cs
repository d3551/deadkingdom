using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private PlayerInputActions playerInput;
    private GameObject pauseUI;
    [SerializeField] private GameObject optionsUI;
    [SerializeField] private GameObject gamepadUI;
    [SerializeField] private GameObject keyboardUI;

    private void Awake()
    {
        playerInput = new PlayerInputActions();
        pauseUI = GameObject.FindGameObjectWithTag("PauseMenu");
    }

    // Enable inputs
    private void OnEnable()
    {
        playerInput.Enable();
    }

    // Disable inputs
    private void OnDisable()
    {
        playerInput.Disable();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        pauseUI.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseUI.SetActive(false);
    }

    public void Options()
    {
        optionsUI.SetActive(true);
    }

    public void Change()
    {
        if (gamepadUI.activeInHierarchy)
        {
            gamepadUI.SetActive(false);
            keyboardUI.SetActive(true);
        }
        else if (keyboardUI.activeInHierarchy)
        {
            gamepadUI.SetActive(true);
            keyboardUI.SetActive(false);
        }
    }

    public void CloseOptions()
    {
        optionsUI.SetActive(false);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
