using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCombat : MonoBehaviour
{

    [SerializeField] Transform attackPoint;
    [SerializeField] float attackRange = 0.4f;
    [SerializeField] LayerMask enemyLayers;
    [SerializeField] float attackDamage = 20f;

    [SerializeField] float attackRate = 2f;
    float nextAttackTime = 0f;
    float secondAttackTime = 0f;
    float thirdAttackTime = 0f;

    private Animator anim;
    [SerializeField] private AudioSource attaqueAudio;
    [SerializeField] private AudioClip attaque1Audio;
    [SerializeField] private AudioClip attaque2Audio;
    [SerializeField] private AudioClip attaque3Audio;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // FixedUpdate is called once per frame
    void FixedUpdate()
    {
        
    }

    // Attack in the designated area
    public void Attack(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (Time.time >= nextAttackTime)
            {
                anim.SetTrigger("Attack1");
                StartCoroutine(WaitAndPlaySound(0, attaque1Audio));
                Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
                foreach (Collider2D enemy in hitEnemies)
                {
                    enemy.GetComponent<EnemyHealth>().TakeDamage(attackDamage);
                }
                nextAttackTime = Time.time + 1f / attackRate;
            }
            else
            {
                if (Time.time >= secondAttackTime)
                {
                    anim.SetTrigger("Attack2");
                    StartCoroutine(WaitAndPlaySound(attaque1Audio.length, attaque2Audio));
                    Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
                    foreach (Collider2D enemy in hitEnemies)
                    {
                        enemy.GetComponent<EnemyHealth>().TakeDamage(attackDamage);
                    }
                    secondAttackTime = Time.time + 1f / attackRate; 
                    nextAttackTime = Time.time + 1f / attackRate;
                }
                else
                {
                    if (Time.time >= thirdAttackTime)
                    {
                        anim.SetTrigger("Attack3");
                        StartCoroutine(WaitAndPlaySound(attaque2Audio.length, attaque3Audio));
                        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
                        foreach (Collider2D enemy in hitEnemies)
                        {
                            enemy.GetComponent<EnemyHealth>().TakeDamage(attackDamage);
                        }
                        secondAttackTime = Time.time + 1f / attackRate;
                        nextAttackTime = Time.time + 1f / attackRate;
                        thirdAttackTime = Time.time + 1f / attackRate;
                    }
                }
            }
        }
    }

    public IEnumerator WaitAndPlaySound(float seconds, AudioClip audio)
    {
        yield return new WaitForSeconds(seconds-1f);
        attaqueAudio.clip = audio;
        attaqueAudio.Play();
    }
}
