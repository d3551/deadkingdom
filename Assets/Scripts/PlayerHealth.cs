using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 200;
    [SerializeField] private float health;
    private SpriteRenderer spriteRenderer;
    private bool canTakeDamage = true;
    [SerializeField] float invincibilityFlashDelay = 0.15f;
    [SerializeField] float invincibilityDuration = 3f;
    [SerializeField] private int lives = 5;
    Renderer rend;

    private Animator anim;

    private void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        rend = GetComponent<Renderer>();

        // Use the Sprite shader on the material
        rend.material.shader = Shader.Find("SpriteShaderEmissive");

        health = maxHealth;
        ColorUpdate();
    }

    public void TakeDamage(float damage)
    {
        if (canTakeDamage)
        {
            if (health - damage <= 0)
            {
                canTakeDamage = false;
                Debug.Log(lives);
                Die();
            }

            if (health - damage > 0)
            {
                anim.SetTrigger("IsHit");
                health -= damage;                
                canTakeDamage = false;
                ColorUpdate();
                StartCoroutine(InvincibilityFlash());
                StartCoroutine(HandleInvincibilityDelay());
            }
        }
    }

    public void Heal(float healing)
    {
        if (health + healing <= maxHealth)
        {
            health += healing;
        }
        else
        {
            health = maxHealth;
        }
        ColorUpdate();
        ChangeSpriteColor();
    }

    public void ChangeSpriteColor()
    {
        spriteRenderer.color = new Color(health/maxHealth, health/maxHealth, health/maxHealth, 1f);
    }

    public void Die()
    {
        anim.SetTrigger("IsDying");
        StartCoroutine(WaitDeath(anim.GetCurrentAnimatorStateInfo(0).length));
    }

    public void AddLife()
    {
        lives++;
    }
    public void RemLife()
    {
        lives--;
        if (lives < 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public IEnumerator WaitDeath(float waitTime)
    {
        yield return new WaitForSeconds(waitTime-0.3f);
        RemLife();
        Heal(maxHealth);
        gameObject.transform.position = GameObject.FindGameObjectWithTag("PlayerSpawn").transform.position;
        canTakeDamage = true;
    }

    public IEnumerator InvincibilityFlash()
    {
        while (!canTakeDamage)
        {
            spriteRenderer.color = new Color(0, 0, 0, 1f);
            yield return new WaitForSeconds(invincibilityFlashDelay);
            spriteRenderer.color = new Color(health/maxHealth, health/maxHealth, health/maxHealth, 1f);
            yield return new WaitForSeconds(invincibilityFlashDelay);
        }
    }

    public IEnumerator HandleInvincibilityDelay()
    {
        yield return new WaitForSeconds(invincibilityDuration);
        canTakeDamage = true;
        ChangeSpriteColor();
    }

    private void ColorUpdate()
    {
        if (health == maxHealth)
        {
            rend.material.SetColor("_Color", new Vector4(0, 1, 0, 1));
            //Debug.Log("full");
        }

        if (health > maxHealth * 1 / 3 && health < maxHealth)
        {

            rend.material.SetColor("_Color", new Vector4(1, 0.6664481f, 0, 1));
            //Debug.Log("2/3");
        }

        if (health > 0 && health <= maxHealth * 2 / 3)
        {

            rend.material.SetColor("_Color", new Vector4(1, 0, 0, 1));
            //Debug.Log("1/3");
        }

    }
}
