using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovements : MonoBehaviour
{

    private PlayerInputActions playerInput;
    private Rigidbody2D rb;
    private float moveInput;
    private SpriteRenderer spriteRenderer;
    private Animator anim;
    [SerializeField] GameObject scythe;

    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] Transform attackPoint;
    [SerializeField] Transform throwPoint;

    [SerializeField] float speed = 10f;
    [SerializeField] float jumpForce = 25f;
    private int canDoubleJump = 1;

    [SerializeField] private GameObject pauseMenuUI;

    // Set default on load
    private void Awake()
    {
        playerInput = new PlayerInputActions();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Enable inputs
    private void OnEnable()
    {
        playerInput.Enable();
    }

    // Disable inputs
    private void OnDisable()
    {
        playerInput.Disable();
    }

    // FixedUpdate is called once per frame
    void FixedUpdate()
    {
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }

    // Check if player is on the ground
    private bool isGrounded()
    {
        return Physics2D.OverlapBox(groundCheck.position, new Vector2(0.4f, 0.2f), 0f, groundLayer);
    }

    // Move the player depending on the direction pressed
    public void Move(InputAction.CallbackContext context)
    {
        moveInput = context.ReadValue<Vector2>().x;
        Flip();
    }

    // Handle jump and double jump
    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            anim.SetTrigger("IsJumping");
            canDoubleJump = 1;
        }
        if(context.performed && !isGrounded() && canDoubleJump > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            anim.SetTrigger("IsJumping");
            canDoubleJump = 0;
        }

        if (context.canceled && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }
    }

    // Flip the sprite and change side for attacks
    void Flip()
    {
        if (moveInput < 0f && this.transform.localScale.x>0 || moveInput > 0f && this.transform.localScale.x<0)
        {
            this.transform.localScale = new Vector3(this.transform.localScale.x*-1, this.transform.localScale.y, this.transform.localScale.z);
        }
    }


    public void Throw(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            anim.SetTrigger("IsThrowing");
            Instantiate(scythe, throwPoint.position, throwPoint.rotation);

        }
    }

    public void Pause(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (!pauseMenuUI.activeInHierarchy)
            {
                pauseMenuUI.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                if (GameObject.Find("OptionsImage")&& GameObject.Find("OptionsImage").activeInHierarchy)
                {
                    GameObject.Find("OptionsImage").SetActive(false);
                }
                pauseMenuUI.SetActive(false);
                Time.timeScale = 1;
            }
        }
    }

    void OnDrawGizmos()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheck.position, new Vector2(0.4f, 0.2f));
    }
}
