using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    [SerializeField] private GameObject player;

    private void Awake()
    {
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            Instantiate(player, transform.position, new Quaternion(0, 0, 0, 0));
            player.GetComponent<Animator>().SetTrigger("IsSpawning");
        }
        else
        {
            GameObject.FindGameObjectWithTag("Player").transform.position = transform.position;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetTrigger("IsSpawning");
        }
    }
}
