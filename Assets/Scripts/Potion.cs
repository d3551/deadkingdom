using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{

    [SerializeField] private float pv = 50;

    [SerializeField] private AudioSource bonusAudio;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            collision.GetComponent<PlayerHealth>().Heal(pv);
            bonusAudio.Play();
            Destroy(this.gameObject);
        }
    }
}
