using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scythe : MonoBehaviour
{

    [SerializeField] float speed = 10f;
    [SerializeField] float scytheDamage = 10f;

    [SerializeField] private AudioSource scytheAudio;

    private Rigidbody2D rb;
    private PolygonCollider2D bc;
    private BoxCollider2D bc2;
    private GameObject player;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<PolygonCollider2D>();
        bc2 = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        GameObject oldScythe = GameObject.FindGameObjectWithTag("Scythe");
        player = GameObject.FindGameObjectWithTag("Player");
        scytheAudio.Play();
        if (oldScythe != null && oldScythe != this.gameObject)
        {
            Destroy(oldScythe);
        }
        if (transform.position.x < player.transform.position.x)
        {
            speed = -speed;
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(speed, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.GetComponent<EnemyHealth>().TakeDamage(scytheDamage);
        }
        if (collision.tag == "Solid" || collision.tag == "Scythe")
        {
            speed = 0;
            bc.isTrigger = false;
            bc2.isTrigger = false;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            anim.enabled = false;
            scytheAudio.Stop();
        }
    }
}
