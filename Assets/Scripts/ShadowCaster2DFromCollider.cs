using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(ShadowCaster2D))]
[DefaultExecutionOrder(100)]
public class ShadowCaster2DFromCollider : MonoBehaviour
{

    static readonly FieldInfo _meshField;
    static readonly FieldInfo _shapePathField;
    static readonly MethodInfo _generateShadowMeshMethod;
    [SerializeField] private SpriteRenderer sprite;



    ShadowCaster2D _shadowCaster;

    EdgeCollider2D _edgeCollider;
    PolygonCollider2D _polygonCollider;
    Vector3[] points;

    private float nextUpdate = 0;
    private float interval = 0.05f;
    private int e = 0;
    private float framesTotal = 0;
    private int frames = 20;
    private GameObject cam;
    private GameObject self;

    private float dist;
 
    private float cullDist = 15;
    static ShadowCaster2DFromCollider()
    {
        _meshField = typeof(ShadowCaster2D).GetField("m_Mesh", BindingFlags.NonPublic | BindingFlags.Instance);
        _shapePathField = typeof(ShadowCaster2D).GetField("m_ShapePath", BindingFlags.NonPublic | BindingFlags.Instance);

        _generateShadowMeshMethod = typeof(ShadowCaster2D).Assembly.GetType("UnityEngine.Experimental.Rendering.Universal.ShadowUtility").GetMethod("GenerateShadowMesh", BindingFlags.Public | BindingFlags.Static);
    }

    private void Start()
    {
        _shadowCaster = GetComponent<ShadowCaster2D>();
        _edgeCollider = GetComponent<EdgeCollider2D>();

        self = gameObject;
        cam = Camera.main.gameObject;


        if (_edgeCollider == null)
            _polygonCollider = GetComponent<PolygonCollider2D>();

        Vector2[] pointsCount = null;
         System.Array.Resize<Vector2>(ref pointsCount, 50);

            
        _polygonCollider.pathCount = 1;

        InvokeRepeating("UpdateEvery", 0f, 0.1f);
    }




    private void UpdateEvery()
    {
        dist = Vector3.Distance(self.transform.position, cam.transform.position);
        if (dist <= cullDist)
        {
            UpdateShadow();
            UpdateCollider();
        }
        

    }
    public void UpdateShadow()
    {

        Vector2[] vertexCollider = _polygonCollider.points;
        Vector2[] vertexColliderOpti = null;

        System.Array.Resize<Vector2>(ref vertexColliderOpti, 200);
        e = 0;
        
        for (int z = 0; z< vertexCollider.Length;)
        {
            vertexColliderOpti[e] = new Vector2(vertexCollider[z].x, vertexCollider[z].y);
            e++;
            z += 2;
        }
        Vector3[] vertexColliderAppend = null;


        int size = vertexColliderOpti.Length;

        for (int i = 0; i < 200; i++)
        {

            System.Array.Resize<Vector3>(ref vertexColliderAppend, size);
            vertexColliderAppend[i] = new Vector3(vertexColliderOpti[i].x, vertexColliderOpti[i].y, 0);
            

        }

        _shapePathField.SetValue(_shadowCaster, vertexColliderAppend);
        _meshField.SetValue(_shadowCaster, new Mesh());
        _generateShadowMeshMethod.Invoke(_shadowCaster, new object[] { _meshField.GetValue(_shadowCaster), _shapePathField.GetValue(_shadowCaster) });
    }

    public void UpdateCollider() 
    { 

        List<Vector2> vertexPosCollider = new List<Vector2>();
        for (int n = 0; n < _polygonCollider.pathCount; n++)
        {
            _polygonCollider.SetPath(n, vertexPosCollider);
            
        }
        _polygonCollider.pathCount = sprite.sprite.GetPhysicsShapeCount();

        List<Vector2> newVertexPosCollider = new List<Vector2>();
        for (int a = 0; a < _polygonCollider.pathCount; a++)
        {
            newVertexPosCollider.Clear();
            sprite.sprite.GetPhysicsShape(a, newVertexPosCollider);
            _polygonCollider.SetPath(a, newVertexPosCollider.ToArray());
        }

    }
}
