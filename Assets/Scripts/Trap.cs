using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{

    private bool activated = true;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (activated)
        {
            if (collision.transform.CompareTag("Player"))
            {
                collision.GetComponent<PlayerHealth>().Die();
                StartCoroutine(ResetWait());
            }
        }
    }

    public IEnumerator ResetWait()
    {
        activated = false;
        yield return new WaitForSeconds(1f);
        activated = true;
    }
}
