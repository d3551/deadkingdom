using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] float maxHealth = 100f;
    float currentHealth;

    [SerializeField] private float speed = 10f;
    [SerializeField] private float damage = 15f;
    [SerializeField] private Transform[] waypoints;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        target = waypoints[0];
    }

    // Reduce life
    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        Debug.Log(this.gameObject.name + " : " +currentHealth);

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    // Launch death animation and disable then destroy
    void Die()
    {

        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;

        StartCoroutine(Despawn(1f));
    }

    // Destroy object after some time
    public IEnumerator Despawn(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }

    private Transform target;
    private int destPoint = 0;

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) < 0.3f)
        {
            destPoint = (destPoint + 1) % waypoints.Length;
            target = waypoints[destPoint];
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            PlayerHealth playerHealth = collision.transform.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
        }
    }
}
